<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Struktur_Model extends CI_Model {

  public $table = 'tb_struktur';
  public $id = 'id';

  public function Get($table){
    $res = $this->db->get($table);
    return $res->result_array();
  }

  function ambil_data_id($id){
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  public function insert($data){
    $this->db->insert('tb_struktur', $data);
    return TRUE;
  }

  public function simpan_struktur($file){
    $data = array(
        'gambar' => $file
    );
    $result = $this->db->insert('tb_struktur', $data);
    return $result;
  }

  public function updateStruktur($data, $case){
    $this->db->update($this->table, $data, $case);
    return TRUE;
  }

  private function deleteImage($id){
    $struktur = $this->ambil_data_id($id);
    $filename = explode(".", $struktur->file)[0];
    return array_map('unlink', glob(FCPATH."./assets/general/$filename.*"));
  }

  public function delete($where){
    $this->db->where($where);
    $this->db->delete($this->table);
    return TRUE;
  }

}
?>
