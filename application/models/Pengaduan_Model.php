<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan_Model extends CI_Model {

  public $table = 'tb_pengaduan';
  public $id = 'id_pengaduan';

  public function Get($table)
  {
    $res = $this->db->get($table);
    return $res->result_array();
  }

  function ambil_data_id($id)
  {
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  public function insert($data)
  {
    $this->db->insert('tb_pengaduan', $data);
    return TRUE;
  }

  public function simpan_pengaduan($nama_ciri, $ruangan, $kritik_saran, $tanggal){
    $data = array(
        'nama_ciri' => $nama_ciri,
        'ruangan' => $ruangan,
        'kritik_saran' => $kritik_saran,
        'tanggal' => $tanggal
    );
    $result = $this->db->insert('tb_pengaduan', $data);
    return $result;
  }
  
}
?>
