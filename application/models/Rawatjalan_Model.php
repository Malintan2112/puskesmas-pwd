<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rawatjalan_Model extends CI_Model {

  public $table = 'tb_rawat_jalan';
  public $id = 'id_rawat_jalan';

  public function Get($table){
    $res = $this->db->get($table);
    return $res->result_array();
  }

  function ambil_data_id($id){
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  public function insert($data){
    $this->db->insert('tb_rawat_jalan', $data);
    return TRUE;
  }

  public function simpanRawatJalan($judul, $tanggal, $file){
    $data = array(
      'judul' => $judul,
      'tanggal' => $tanggal,
      'gambar' => $file
    );
    $result = $this->db->insert('tb_rawat_jalan', $data);
    return $result;
  }

  public function updateRawatJalan($data, $case){
    $this->db->update($this->table, $data, $case);
    return TRUE;
  }

  private function deleteImage($id){
    $rawatJalan = $this->ambil_data_id($id);
    $filename = explode(".", $rawatJalan->file)[0];
    return array_map('unlink', glob(FCPATH."./assets/rawat-jalan/$filename.*"));
  }

  public function delete($where){
    $this->db->where($where);
    $this->db->delete($this->table);
    return TRUE;
  }

}
?>
