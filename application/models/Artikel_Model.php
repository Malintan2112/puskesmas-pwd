<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel_Model extends CI_Model {

  public $table = 'tb_artikel';
  public $id = 'id_artikel';

  public function Get($table){
    $this->db->order_by('no',"DESC");
    $res = $this->db->get($table);
    return $res->result_array();
  }

  public function GetFE($table){
    $this->db->where_not_in('tayang',0);
    $this->db->order_by('no',"DESC");
    $res = $this->db->get($table);
    return $res->result_array();
  }

  public function Get3_latest($table){
    $this->db->where_not_in('tayang',0);
    $this->db->limit(3);
    $this->db->order_by('no',"DESC");
    $res = $this->db->get($table);
    return $res->result_array();
  }

  function ambil_data_id($id){
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  public function insert($data){
    $this->db->insert('tb_artikel', $data);
    return TRUE;
  }

  public function simpan_artikel($judul, $isi, $file){
    $data = array(
        'judul' => $judul,
        'isi' => $isi,
        'gambar' => $file
    );
    $result = $this->db->insert('tb_artikel', $data);
    return $result;
  }

  function edit_data($id, $data){
    $this->db->where($this->id, $id);
    $this->db->update($this->table,$data);
  }

  private function deleteImage($id){
    $artikel = $this->ambil_data_id($id);
    $filename = explode(".", $artikel->file)[0];
    return array_map('unlink', glob(FCPATH."./assets/artikel/$filename.*"));
  }

  public function tampilUbahArtikel($id){
    return $this->db->select('*')
                    ->where('id_artikel', $id)
                    ->get($this->table);
  }

  public function updateArtikel($data, $case){
    $this->db->update($this->table, $data, $case);
    return TRUE;
  }

  public function delete($where){
    $this->db->where($where);
    $this->db->delete($this->table);
    return TRUE;
  }

}
?>
