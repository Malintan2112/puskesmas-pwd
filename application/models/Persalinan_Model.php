<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persalinan_Model extends CI_Model {

  public $table = 'tb_persalinan';
  public $id = 'id_persalinan';

  public function Get($table){
    $res = $this->db->get($table);
    return $res->result_array();
  }

  function ambil_data_id($id){
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  public function insert($data){
    $this->db->insert('tb_persalinan', $data);
    return TRUE;
  }

  public function simpanPersalinan($judul, $tanggal, $file){
    $data = array(
        'judul' => $judul,
        'tanggal' => $tanggal,
        'gambar' => $file
    );
    $result = $this->db->insert('tb_persalinan', $data);
    return $result;
  }

  public function updatePersalinan($data, $case){
    $this->db->update($this->table, $data, $case);
    return TRUE;
  }

  private function deleteImage($id){
    $persalinan = $this->ambil_data_id($id);
    $filename = explode(".", $persalinan->file)[0];
    return array_map('unlink', glob(FCPATH."./assets/persalinan/$filename.*"));
  }

  public function delete($where){
    $this->db->where($where);
    $this->db->delete($this->table);
    return TRUE;
  }

}
?>
