<?php
    $this->load->view('navbar', $menu);
    $this->load->view('header', $title);
    ?>

    <div class="container">
      <br>
      <div class="row">
        <div class="col-lg-12">
          <h3>Program Puskesmas</h3>
          <?php foreach ($data as $program): echo htmlspecialchars_decode(stripcslashes($program['isi'])); endforeach; ?>
        </div>
      </div>
      <br><br>
    </div>

<?php
    $this->load->view('footer');
?>