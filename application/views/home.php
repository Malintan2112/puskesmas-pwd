<?php
  $this->load->view('navbar');
?>

  <!-- Start Hero Seciton -->
  <div class="st-hero-wrap st-gray-bg st-dynamic-bg overflow-hidden st-fixed-bg" data-src="assets/background/<?php foreach ($dataBackground as $background): echo $background['gambar']; endforeach; ?>">
    <div class="st-hero st-style1">
      <div class="container">
        <div class="st-hero-text">
          <div class="st-height-b40 st-height-lg-b40"></div>
          <h1 class="st-hero-title cd-headline slide wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s">
            UPTD Puskesmas <br>
            <span class="cd-words-wrapper">
              <b class="is-visible">Purwodadi I &nbsp; </b>
              <b style="color: #009795">Purwodadi I &nbsp; </b>
            </span>
          </h1>
          <div class="st-hero-subtitle wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">Jl. Gajah Mada No. 1 Purwodadi<br>
            Kabupaten Grobogan 58111</div>
        </div>
      </div>
    </div>
    <div class="st-slider st-style1 st-hero-slider1" id="home">
      <div class="slick-container" data-autoplay="1" data-loop="1" data-speed="800" data-autoplay-timeout="1000"
        data-center="0" data-slides-per-view="1" data-fade-slide="1">
        <div class="slick-wrapper">
          <div class="slick-slide-in">
            <div class="st-hero-img">
              <img src="assets/img/logo-grobogan.png" alt="Hero img">
            </div>
          </div>
          <div class="slick-slide-in">
            <div class="st-hero-img">
              <img src="assets/img/logo-puskesmas.png" alt="Hero img">
            </div>
          </div>
        </div>
      </div><!-- .slick-container -->
      <div class="pagination st-style1 container"></div> <!-- If dont need Pagination then add class .st-hidden -->
      <div class="swipe-arrow st-style1 st-hidden">
        <!-- If dont need navigation then add class .st-hidden -->
        <div class="slick-arrow-left"><i class="fa fa-angle-left"></i></div>
        <div class="slick-arrow-right"><i class="fa fa-angle-right"></i></div>
      </div>
    </div><!-- .st-slider -->
    <div class="st-hero-shape"><img src="assets/img/shape/hero-shape.png" alt="hero shape"></div>
  </div>
  <!-- End Hero Seciton -->

  <!-- Start Jam Pelayanan Seciton -->
  <section class="st-about-wrap" id="about">
    <div class="st-shape-bg">
      <img src="assets/img/shape/about-bg-shape.svg" alt="shape">
    </div>
    <br>
    <div class="container">
      <div class="st-section-heading st-style1">
        <h2 class="st-section-heading-title">Jam Pelayanan</h2>
        <div class="st-seperator">
          <div class="st-seperator-left wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s"></div>
          <div class="st-seperator-center"><img src="assets/img/icon.png" alt="icon"></div>
          <div class="st-seperator-right wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s"></div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8 wow fadeInRight" data-wow-duration="0.8s" data-wow-delay="0.2s">
          <div class="st-shedule-wrap">
            <div class="st-shedule">
              <table style="width: 100%;">
                <tr style="border-bottom: rgb(17, 193, 178) 0.4px solid;">
                  <th style="padding: 8px 0;">Jenis Pelayanan</th>
                  <th>Hari</th>
                  <th>Jam</th>
                </tr>
                <tr>
                  <td>Pendaftaran</td>
                  <td>Senin – Kamis<br>Jumat<br>Sabtu</td>
                  <td>07 : 30 – 12 : 00<br>07 : 30 – 10 : 00<br>07 : 30 – 11 : 00</td>
                </tr>
                <tr>
                  <td>Pemeriksaan Umum</td>
                  <td>Setiap Hari Kerja</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Kesehatan Gigi dan Mulut</td>
                  <td>Setiap Hari Kerja</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Pelayanan Tindakan</td>
                  <td>Setiap Hari Kerja</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>KIA</td>
                  <td>Setiap Hari Kerja</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Pelayanan KB</td>
                  <td>Rabu & Jumat</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>MTBS</td>
                  <td>Setiap Hari Kerja</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Imunisasi</td>
                  <td>Senin</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Persalinan</td>
                  <td colspan="2" style="text-align: center;">24 Jam</td>
                </tr>
                <tr>
                  <td>Fisioterapi</td>
                  <td>Setiap Hari Kerja</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Konseling PKPR</td>
                  <td>Senin & Rabu</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Konseling TB</td>
                  <td>Setiap Hari Kerja</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Konseling HIV</td>
                  <td>Selasa & Kamis</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Konseling Gizi</td>
                  <td>Selasa & Kamis</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>IVA</td>
                  <td>Kamis</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Laborat</td>
                  <td>Setiap Hari Kerja</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
                <tr>
                  <td>Farmasi</td>
                  <td>Setiap Hari Kerja</td>
                  <td>08 : 00 – Selesai</td>
                </tr>
              </table>
            </div>
          </div>
        </div><!-- .col -->
      </div>
    </div>
  </section>
  <!-- End Jam Pelayanan Seciton -->

  <!-- Start artikel Section -->
  <?php if (count($dataArtikel) > 0) { ?>   
  <section id="artikel">
    <div class="st-height-b70 st-height-lg-b80"></div>
    <div class="container">
      <div class="st-section-heading st-style1">
        <h2 class="st-section-heading-title">Artikel Kesehatan</h2>
        <div class="st-seperator">
          <div class="st-seperator-left wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s"></div>
          <div class="st-seperator-center"><img src="assets/img/icon.png" alt="icon"></div>
          <div class="st-seperator-right wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s"></div>
        </div>
      </div>
      <div class="st-height-b40 st-height-lg-b40"></div>
    </div>
    <div class="container">
      <div class="row">
        <?php foreach ($dataArtikel as $artikel): ?>
          <div class="col-lg-4">
            <div class="st-post st-style3 st-zoom">
              <a href="<?= base_url('artikel/detail/'.$artikel['id_artikel']) ?>" class="st-post-thumb" style="max-height: 300px; background-position: center;">
                <img class="st-zoom-in" src="assets/artikel/<?= $artikel['gambar']; ?>" alt="blog1">
              </a>
              <div class="st-post-info">
                <h2 class="st-post-title"><a href="<?= base_url('artikel/detail/'.$artikel['id_artikel']) ?>"><?= $artikel['judul']; ?></a></h2>
                <div class="st-post-meta">
                  <span class="st-post-date"><?= date('d-m-Y', strtotime($artikel['tanggal'])); ?></span>
                </div>
                <!-- <div class="st-post-text"><?= htmlspecialchars_decode(stripcslashes($artikel['isi'])) . '...'; ?></div> -->
              </div>
              <div class="st-post-footer" style="margin-top: 15px">
                <a href="<?= base_url('artikel/detail/'.$artikel['id_artikel']) ?>" class="st-btn st-style2 st-color1 st-size-medium">Selengkapnya</a>
              </div>
            </div>
            <div class="st-height-b30 st-height-lg-b30"></div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
    <br>
  </section>
  <?php } ?>
  <!-- End artikel Section -->
  <div class="st-height-b50 st-height-lg-b80"></div>

<?php
  $this->load->view('footer');
?>