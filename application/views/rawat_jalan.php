<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <!-- Start Section -->
  <?php foreach ($data as $rawat_jalan): ?>
  <section>
    <br><br>
    <div class="container">
      <div class="st-section-heading st-style1">
        <h3 class="st-section-heading-title"><?= $rawat_jalan['judul']; ?></h3>
        <div class="st-seperator">
          <div class="st-seperator-left wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s"></div>
          <div class="st-seperator-center"><img src="assets/img/icon.png" alt="icon"></div>
          <div class="st-seperator-right wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s"></div>
        </div>
        <br>
        <div class="row">
          <div class="col-lg-2"></div>
          <div class="col-lg-8">
            <div class="st-post-details st-style1">
              <a href="<?= base_url('assets/general/'.$rawat_jalan['gambar']) ?>" target="_blank">
                <img src="assets/general/<?= $rawat_jalan['gambar']; ?>">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div><br>
  </section>
  <?php endforeach; ?>
  <!-- End Section -->

<?php
  $this->load->view('footer');
?>