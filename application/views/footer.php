</div>
 
  <!-- Start Footer -->
  <footer class="st-site-footer st-sticky-footer st-dynamic-bg" data-src="<?php echo base_url(); ?>assets/img/footer-bg.png">
    <div class="st-main-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-3">
            <div class="st-footer-widget">
              <div class="st-text-field">
                <img src="<?php echo base_url(); ?>assets/img/logo.png" class="st-footer-logo" style="max-width: 190px;">
                <div class="st-height-b25 st-height-lg-b25"></div>
                <div class="st-footer-text">Jl. Gajah Mada No. 1 Purwodadi <br>Kabupaten Grobogan 58111</div>
                <div class="st-height-b25 st-height-lg-b25"></div>
                <ul class="st-social-btn st-style1 st-mp0">
                  <li><a href="https://www.facebook.com/puskesmas.puskesmas.545" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
                  <li><a href="https://www.instagram.com/puskesmaspurwodadi1" target="_blank"><i class="fab fa-instagram-square"></i></a></li>
                  <li><a href="https://twitter.com/IPuskesmas" target="_blank"><i class="fab fa-twitter-square"></i></a></li>
                  <li><a href="https://wa.me/082324240424" target="_blank"><i class="fab fa-whatsapp-square"></i></a></li>
                  <li><a href="mailto:puskesmas_purwodadi1@yahoo.com" target="_blank"><i class="fas fa-envelope-square"></i></a></li>
                  <li><a href="tel:0292421541" target="_blank"><i class="fas fa-phone-square"></i></a></li>
                </ul>
              </div>
            </div>
          </div><!-- .col -->
          <div class="col-lg-1"></div>
          <div class="col-lg-3">
            <div class="st-footer-widget">
              <h2 class="st-footer-widget-title">Links</h2>
              <ul class="st-footer-widget-nav st-mp0">
                <li><a href="<?php echo base_url(); ?>"><i class="fas fa-chevron-right"></i>Home</a></li>
                <li><a href="<?php echo base_url(); ?>profil"><i class="fas fa-chevron-right"></i>Profil</a></li>
                <li><a href="<?php echo base_url(); ?>pelayanan"><i class="fas fa-chevron-right"></i>Pelayanan</a></li>
                <li><a href="<?php echo base_url(); ?>program"><i class="fas fa-chevron-right"></i>Program</a></li>
              </ul>
            </div>
          </div><!-- .col -->
          <div class="col-lg-3">
            <div class="st-footer-widget">
              <h2 class="st-footer-widget-title">&nbsp;</h2>
              <ul class="st-footer-widget-nav st-mp0">
                <li><a href="<?php echo base_url(); ?>galeri"><i class="fas fa-chevron-right"></i>Galeri</a></li>
                <li><a href="<?php echo base_url(); ?>pengaduan"><i class="fas fa-chevron-right"></i>Pengaduan Saran</a></li>
                <li><a href="<?php echo base_url(); ?>kontak"><i class="fas fa-chevron-right"></i>Hubungi Kami</a></li>
              </ul>
            </div>
          </div><!-- .col -->
        </div>
      </div>
    </div>
    <div class="st-copyright-wrap">
      <div class="container">
        <div class="st-copyright-in">
          <div class="st-left-copyright">
            <div class="st-copyright-text">Copyright 2022. Created by IBRA Production</div>
          </div>
          <div class="st-right-copyright">
            <div id="st-backtotop"><i class="fas fa-angle-up"></i></div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End Footer -->

  <!-- Scripts -->
  <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/isotope.pkg.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery.slick.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/counter.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/lightgallery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/ripples.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jQueryUi.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/textRotate.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html>