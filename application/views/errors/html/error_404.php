<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>404 Page Not Found</title>
<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,700,800,900&display=swap" rel="stylesheet">
<style type="text/css">
	body {
		background: rgb(161,194,122);
		background: linear-gradient(90deg, rgba(161,194,122,1) 0%, rgba(0,120,119,1) 92%);
		text-align: center;
		margin-top: 20%;
		color: white;
		font-family: Poppins, sans-serif;
	}
	@media screen and (min-width: 601px) {
		p {
			font-size: 30px;
		}
	}
	@media screen and (max-width: 600px) {
		.text {
			padding: 65% 7px 0;
		}
		p {
			font-size: 22px !important;
		}
	}
</style>
</head>
<body>
	<div class="text">
		<p>Maaf, Halaman yang anda cari tidak ditemukan !</p>
	</div>
</body>
</html>