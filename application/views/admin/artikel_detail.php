<?php
$this->load->view('admin/header');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Artikel</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-body">

                <div class="post">
                  <div class="user-block">
                    <h4 class="" style="margin-left:0">
                      <a href="#"><?= $judul; ?></a>
                    </h4>
                    <span class="description" style="margin-left:0">Di update tanggal: <?= date('d-m-Y H:i', strtotime($tanggal)); ?> | Tayang: <b><?= $tayang ? 'Ya' : 'Tidak'; ?></b></span>
                  </div>
                  <img src="../../../assets/artikel/<?= $gambar; ?>" style="max-height: 280px;margin:15px 0;"><br>
                </div>
                <?= htmlspecialchars_decode(stripcslashes($isi)) ?>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>