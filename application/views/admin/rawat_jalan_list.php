<?php
  $this->load->view('admin/header');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Rawat Jalan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-body">
              <a href="<?php echo base_url(); ?>admin/rawat_jalan/tambah" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data"><i class="fa fa-plus"></i> TAMBAH DATA</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th scope="col" width="25px">No</th>
                    <th>Judul</th>
                    <th>Terakhir Diupdate</th>
                    <th>Gambar</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $no=1; foreach ($data as $rawat_jalan): ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $rawat_jalan['judul']; ?></td>
                    <td><?= date('d-m-Y H:i', strtotime($rawat_jalan['tanggal'])); ?></td>
                    <td>
                      <a href="../assets/general/<?= $rawat_jalan['gambar']; ?>" target="_blank">
                        <img src="../assets/general/<?= $rawat_jalan['gambar']; ?>" style="max-width: 65px;max-height: 65px;">
                      </a>
                    </td>
                    <td>
                      <a href="<?= site_url('admin/rawat_jalan/edit/'.$rawat_jalan['id_rawat_jalan']) ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit Rawat Jalan">EDIT</a>
                      <a href="<?= site_url('admin/rawat_jalan/hapus?id='.$rawat_jalan['id_rawat_jalan'].'&file='.$rawat_jalan['gambar']) ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Rawat Jalan">HAPUS</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>