<?php
  $this->load->view('admin/header');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Pengaduan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th scope="col" width="25px">No</th>
                    <th>Nama / Ciri Pegawai</th>
                    <th>Nama Ruangan</th>
                    <th>Kritik Saran</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $no=1; foreach ($data as $pengaduan): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $pengaduan['nama_ciri']; ?></td>
                    <td><?php echo $pengaduan['ruangan']; ?></td>
                    <td><?php echo substr($pengaduan['kritik_saran'], 0, 25) . '...'; ?></td>
                    <td>
                      <a href="<?php echo site_url('admin/pengaduan/detail/'.$pengaduan['id_pengaduan']) ?>" class="btn btn-sm btn-primary" >DETAIL</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>
