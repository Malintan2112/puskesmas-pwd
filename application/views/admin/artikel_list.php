<?php
  $this->load->view('admin/header');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Artikel</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-body">
                <a href="<?php echo base_url(); ?>admin/artikel/tambah" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Posting Artikel"><i class="fa fa-plus"></i> POSTING ARTIKEL</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th scope="col" width="25px">No</th>
                    <th>Judul Artikel</th>
                    <th>Terakhir Diupdate</th>
                    <th>Gambar</th>
                    <th>Tayang</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $no=1; foreach ($data as $artikel): ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $artikel['judul']; ?></td>
                        <td><?= date('d-m-Y H:i', strtotime($artikel['tanggal'])); ?></td>
                        <td>
                        <a href="../assets/artikel/<?= $artikel['gambar']; ?>" target="_blank">
                            <img src="../assets/artikel/<?= $artikel['gambar']; ?>" style="max-width: 65px;max-height: 65px;">
                        </a>
                        </td>
                        <td><?= $artikel['tayang'] ? 'Ya' : 'Tidak'; ?></td>
                        <td>
                          <a href="<?= site_url('admin/artikel/detail/'.$artikel['id_artikel']) ?>" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail Artikel">DETAIL</a>
                          <a href="<?= site_url('admin/artikel/edit/'.$artikel['id_artikel']) ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit Artikel">EDIT</a>
                          <a href="<?= site_url('admin/artikel/hapus?id='.$artikel['id_artikel'].'&file='.$artikel['gambar']) ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Artikel">HAPUS</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>