<?php
  $this->load->view('admin/header');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Pengaduan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <tr>
                    <th width="210px">Nama / Ciri Pegawai</th>
                    <td><?= $nama_ciri; ?></td>
                  </tr>
                  <tr>
                    <th width="210px">Nama Ruangan</th>
                    <td><?= $ruangan; ?></td>
                  </tr>
                  <tr>
                    <th width="210px">Kritik Saran</th>
                    <td><?= $kritik_saran; ?></td>
                  </tr>
                  <tr>
                    <th width="210px">Tanggal Dikirim</th>
                    <td><?= date('d-m-Y H:i', strtotime($tanggal)); ?></td>
                  </tr>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>