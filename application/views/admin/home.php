<?php
  $this->load->view('admin/header');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>
                <?php
                $query = $this->db->query('SELECT * FROM tb_artikel');
                echo $query->num_rows();
                ?>
                </h3>
                <p>Data Artikel</p>
              </div>
              <div class="icon">
                <i class="nav-icon fas fa-book"></i>
              </div>
              <a href="<?php echo base_url(); ?>admin/artikel" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>
                <?php
                $query = $this->db->query('SELECT * FROM tb_galeri');
                echo $query->num_rows();
                ?>
                </h3>
                <p>Data Galeri</p>
              </div>
              <div class="icon">
              <i class="nav-icon fas fa-image"></i>
              </div>
              <a href="<?php echo base_url(); ?>admin/galeri" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>
                <?php
                $query = $this->db->query('SELECT * FROM tb_pengaduan');
                echo $query->num_rows();
                ?>
                </h3>
                <p>Data Pengaduan</p>
              </div>
              <div class="icon">
              <i class="nav-icon fas fa-comment"></i>
              </div>
              <a href="<?php echo base_url(); ?>admin/pengaduan" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>