<?php
  $this->load->view('admin/header');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Persalinan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-body">
              <a href="<?php echo base_url(); ?>admin/persalinan/tambah" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Tambah Data"><i class="fa fa-plus"></i> TAMBAH DATA</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th scope="col" width="25px">No</th>
                    <th>Judul</th>
                    <th>Terakhir Diupdate</th>
                    <th>Gambar</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $no=1; foreach ($data as $persalinan): ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $persalinan['judul']; ?></td>
                    <td><?= date('d-m-Y H:i', strtotime($persalinan['tanggal'])); ?></td>
                    <td>
                      <a href="../assets/general/<?= $persalinan['gambar']; ?>" target="_blank">
                        <img src="../assets/general/<?= $persalinan['gambar']; ?>" style="max-width: 65px;max-height: 65px;">
                      </a>
                    </td>
                    <td>
                      <a href="<?= site_url('admin/persalinan/edit/'.$persalinan['id_persalinan']) ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit Persalinan">EDIT</a>
                      <a href="<?= site_url('admin/persalinan/hapus?id='.$persalinan['id_persalinan'].'&file='.$persalinan['gambar']) ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Persalinan">HAPUS</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>