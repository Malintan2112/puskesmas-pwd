<?php
$this->load->view('admin/header');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Data Rawat Jalan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <form action="<?php echo base_url()?>admin/rawat_jalan/updateData" method="post" enctype="multipart/form-data">
              <input type="hidden" name="id_rawat_jalan" value="<?= $id_rawat_jalan; ?>">
              <input type="hidden" name="file-lama" value="<?= $gambar; ?>">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label>Judul</label>
                      <input type="text" class="form-control" placeholder="Judul" value="<?= $judul;?>" name="judul" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Gambar</label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" name="file" value="<?= $gambar;?>">
                      <label class="custom-file-label"><?= $gambar;?></label>
                    </div>
                  </div>
                  <p class="help-block">Max. 5 Mb</p>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="simpan">SIMPAN</button>
              </div>
            </form>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>