<?php
  $this->load->view('admin/header');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Galeri</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-body">
              <a href="<?php echo base_url(); ?>admin/galeri/tambah" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Posting Galeri"><i class="fa fa-plus"></i> POSTING GALERI</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th scope="col" width="25px">No</th>
                    <th>Keterangan</th>
                    <th>Terakhir Diupdate</th>
                    <th>Gambar</th>
                    <th>Tayang</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $no=1; foreach ($data as $galeri): ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $galeri['keterangan']; ?></td>
                    <td><?= date('d-m-Y H:i', strtotime($galeri['tanggal'])); ?></td>
                    <td>
                      <a href="../assets/galeri/<?= $galeri['gambar']; ?>" target="_blank">
                        <img src="../assets/galeri/<?= $galeri['gambar']; ?>" style="max-width: 65px;max-height: 65px;">
                      </a>
                    </td>
                    <td><?= $galeri['tayang'] ? 'Ya' : 'Tidak' ; ?></td>
                    <td>
                      <a href="<?= site_url('admin/galeri/edit/'.$galeri['id_galeri']) ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit Galeri">EDIT</a>
                      <a href="<?= site_url('admin/galeri/hapus?id='.$galeri['id_galeri'].'&file='.$galeri['gambar']) ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Galeri">HAPUS</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('admin/footer');
?>