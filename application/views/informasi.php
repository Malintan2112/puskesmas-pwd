<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <div class="container">
    <br>
    <div class="row">
      <div class="col-lg-12">
        <h3 style="border-bottom: rgb(17, 193, 178) 0.4px solid; padding-bottom: 12px">Subkategori</h3>
        <table style="width: 100%;">
          <tr>
            <td><a href="<?php echo base_url(); ?>galeri">Galeri</a></td>
          </tr>
          <tr>
            <td><a href="<?php echo base_url(); ?>rawat_jalan">Pelayanan Rawat Jalan</a></td>
          </tr>
          <tr>
            <td><a href="<?php echo base_url(); ?>persalinan">Pelayanan Persalinan</a></td>
          </tr>
          <?php if(count($data) > 0) { ?>
          <tr>
            <td><a href="<?php echo base_url(); ?>artikel">Artikel Kesehatan</a></td>
          </tr>
          <?php } ?>
        </table>
      </div>
    </div>
    <br><br>
  </div>

  <div class="st-height-b50 st-height-lg-b80"></div>

<?php
    $this->load->view('footer');
?>