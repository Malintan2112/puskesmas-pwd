<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <div class="container">
  <br><br>
    <div class="row">
      <?php foreach ($dataArtikel as $artikel): ?>
        <div class="col-lg-4">
          <div class="st-post st-style3 st-zoom">
            <a href="<?= base_url('artikel/detail/'.$artikel['id_artikel']) ?>" class="st-post-thumb" style="max-height: 300px; background-position: center;">
              <img class="st-zoom-in" src="assets/artikel/<?= $artikel['gambar']; ?>" alt="blog1">
            </a>
            <div class="st-post-info">
              <h2 class="st-post-title"><a href="<?= base_url('artikel/detail/'.$artikel['id_artikel']) ?>"><?= $artikel['judul']; ?></a></h2>
              <div class="st-post-meta">
                <span class="st-post-date"><?= date('d-m-Y', strtotime($artikel['tanggal'])); ?></span>
              </div>
              <!-- <div class="st-post-text"><?= htmlspecialchars_decode(stripcslashes($artikel['isi'])) . '...'; ?></div> -->
            </div>
            <div class="st-post-footer">
              <a href="<?= base_url('artikel/detail/'.$artikel['id_artikel']) ?>" class="st-btn st-style2 st-color1 st-size-medium">Selengkapnya</a>
            </div>
          </div>
          <div class="st-height-b30 st-height-lg-b30"></div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
  <div class="st-height-b50 st-height-lg-b80"></div>

<?php
  $this->load->view('footer');
?>