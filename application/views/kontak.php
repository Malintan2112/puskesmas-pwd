<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <!-- Start Hubungi Kami Section -->
  <section>
    <br>
    <div class="container">
      <div class="st-section-heading st-style1">
        <br>
        <div class="row">
          <div class="col-lg-4" style="margin-bottom: 30px;">
            <a href="https://www.facebook.com/puskesmas.puskesmas.545" target="_blank">
              <div class="st-iconbox st-style1">
                <div class="st-iconbox-icon st-blue-box">
                  <i class="fab fa-facebook-square"></i>
                </div>
                <h2 class="st-iconbox-title">Facebook</h2>
                <div class="st-iconbox-text">Puskesmas Purwodadi I</div>
              </div>
            </a>
            <div class="st-height-b0 st-height-lg-b30"></div>
          </div>
          <div class="col-lg-4">
            <a href="https://www.instagram.com/puskesmaspurwodadi1" target="_blank">
              <div class="st-iconbox st-style1">
                <div class="st-iconbox-icon st-red-box">
                  <i class="fab fa-instagram-square"></i>
                </div>
                <h2 class="st-iconbox-title">Instagram</h2>
                <div class="st-iconbox-text">puskesmaspurwodadi1</div>
              </div>
            </a>
            <div class="st-height-b0 st-height-lg-b30"></div>
          </div>
          <div class="col-lg-4">
            <a href="https://twitter.com/IPuskesmas" target="_blank">
              <div class="st-iconbox st-style1">
                <div class="st-iconbox-icon st-blue-box">
                  <i class="fab fa-twitter-square"></i>
                </div>
                <h2 class="st-iconbox-title">Twitter</h2>
                <div class="st-iconbox-text">Puskesmas Purwodadi 1</div>
              </div>
            </a>
            <div class="st-height-b0 st-height-lg-b30"></div>
          </div>
          <div class="col-lg-4">
            <a href="https://wa.me/082324240424" target="_blank">
              <div class="st-iconbox st-style1">
                <div class="st-iconbox-icon st-blue-box">
                  <i class="fab fa-whatsapp-square"></i>
                </div>
                <h2 class="st-iconbox-title">WhatsApp</h2>
                <div class="st-iconbox-text">082324240424</div>
              </div>
            </a>
            <div class="st-height-b0 st-height-lg-b30"></div>
          </div>
          <div class="col-lg-4">
            <a href="mailto:puskesmas_purwodadi1@yahoo.com" target="_blank">
              <div class="st-iconbox st-style1">
                <div class="st-iconbox-icon st-red-box">
                  <i class="fas fa-envelope-square"></i>
                </div>
                <h2 class="st-iconbox-title">Email</h2>
                <div class="st-iconbox-text">puskesmas_purwodadi1@yahoo.com</div>
              </div>
            </a>
            <div class="st-height-b0 st-height-lg-b30"></div>
          </div>
          <div class="col-lg-4">
            <a href="tel:0292421541" target="_blank">
              <div class="st-iconbox st-style1">
                <div class="st-iconbox-icon st-blue-box">
                  <i class="fas fa-phone-square"></i>
                </div>
                <h2 class="st-iconbox-title">Telpon</h2>
                <div class="st-iconbox-text">(0292) 421541</div>
              </div>
            </a>
            <div class="st-height-b0 st-height-lg-b30"></div>
          </div>
        </div>
      </div>
    </div>
    <br><br>
  </section>
  <!-- End Hubungi Kami Section -->
  
  <div class="st-google-map">
    <iframe
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3959.248348229386!2d110.90998931459386!3d-7.0971874948746425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e70b00d03b89659%3A0xda90c8467cc11ee9!2sPuskesmas%20Purwodadi%201!5e0!3m2!1sen!2sid!4v1660133995790!5m2!1sen!2sid"
      allowfullscreen></iframe>
  </div>

<?php
    $this->load->view('footer');
?>