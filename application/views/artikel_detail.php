<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>
  
    <div class="container">
      <br>
      <div class="row">
        <div class="col-lg-12">
          <div class="st-post-details st-style1">
            <h2><?= $judul ?></h2><br>
            <div class="st-post-details st-style1">
            <div class="row">
              <div class="col-lg-8">
                <img class="st-zoom-in" src="<?php echo base_url(); ?>assets/artikel/<?= $gambar ?>" style="width: 100%" alt="detail-artikel">
              </div>
            </div>
            </div>
            <div class="st-height-b20 st-height-lg-b20"></div>
            <?= htmlspecialchars_decode(stripcslashes($isi)) ?>
            </div>
        </div>
      </div>
      <br><br>
    </div>

<?php
    $this->load->view('footer');
?>