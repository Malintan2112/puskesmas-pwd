<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <!-- Start galeri Section -->
  <?php if (count($data) > 0) { ?> 
    <section id="galeri">
      <br><br>
      <div class="container">
        <div class="st-portfolio-wrapper">
          <div class="st-isotop st-style1 st-port-col-3 st-has-gutter st-lightgallery">
            <div class="st-grid-sizer"></div>
            <?php foreach ($data as $galeri): ?>
              <div class="st-isotop-item">
                <a href="<?= base_url('assets/galeri/'.$galeri['gambar']) ?>" class="st-project st-zoom st-lightbox-item st-link-hover-wrap">
                  <div class="st-project-img st-zoom-in"><img src="<?= base_url('assets/galeri/'.$galeri['gambar']) ?>" alt="project1"></div>
                  <span class="st-link-hover"><i class="fas fa-arrows-alt"></i></span>
                </a>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
      <div class="st-height-b70 st-height-lg-b80"></div>
    </section>
  <?php } else { ?>
    <div class="text-center mt-4 mb-5">
      <h3>Belum ada galeri</h1>
    </div>
  <?php } ?>
  <!-- End galeri Section -->

<?php
  $this->load->view('footer');
?>