<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    Login | UPTD Puskesmas Purwodadi I
  </title>
  <!-- Favicon Icon -->
  <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png" />
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/login/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/login/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/login/dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page" style="  background: rgb(161,194,122); background: linear-gradient(90deg, rgba(161,194,122,1) 0%, rgba(0,120,119,1) 92%);">
<div class="login-box">
  <div class="card">
      <div class="card-body login-card-body">
        <br>
      <div class="text-center">
        <img src="<?php echo base_url(); ?>assets/img/logo.png" width="200px" />
      </div>
      <br><br>
      <form action="<?php echo base_url() ?>login" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Login</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <br>
      <div class="text-center">
          <?= $this->session->flashdata('message'); ?>
      </div>
      <div class="text-center">
        <a href="<?php echo base_url(); ?>">Kembali ke Halaman Utama</a>
      </div>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/login/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/login/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/login/dist/js/adminlte.min.js"></script>
</body>
</html>
