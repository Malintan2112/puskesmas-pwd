<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <!-- Start Tata Nilai Section -->
  <section class="st-shape-wrap">
    <div class="st-shape1"><img src="assets/img/shape/contact-shape1.svg" alt="shape1"></div>
    <div class="st-shape2"><img src="assets/img/shape/contact-shape2.svg" alt="shape2"></div>
    <div class="st-height-b120 st-height-lg-b80"></div>
    <div class="container">
      <div class="st-section-heading st-style1">
        <h2 class="st-section-heading-title">Tata Nilai</h2>
        <div class="st-seperator">
          <div class="st-seperator-left wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s"></div>
          <div class="st-seperator-center"><img src="assets/img/icon.png" alt="icon"></div>
          <div class="st-seperator-right wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s"></div>
        </div>
        <br>
        <h3 class="st-iconbox-title">" C E R I A "</h3>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-9">
            <div class="wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.2s" style="text-align: left">
              <div class="st-section-heading-subtitle"><i class="fas fa-caret-right"></i>&nbsp;
                <b>C</b>epat Memberikan pelayanan dengan cekatan sesuai dengan tugas pokok dan fungsinya 
              </div>
              <div class="st-section-heading-subtitle"><i class="fas fa-caret-right"></i>&nbsp;
                <b>E</b>mpati Memberikan pelayanan seolah ikut merasakan saat konsumen membutuhkan pelayanan puskesmas
              </div>
              <div class="st-section-heading-subtitle"><i class="fas fa-caret-right"></i>&nbsp;
                <b>R</b>amah Memberikan pelayanan yang baik dan menarik budi bahasanya, manis tutur kata dan sikapnya
              </div>
              <div class="st-section-heading-subtitle"><i class="fas fa-caret-right"></i>&nbsp;
                <b>I</b>novatif Memberikan pelayanan yang kreatif, memodifikasi lingkungan dalam memberikan pelayanan kepada masyarakat
              </div>
              <div class="st-section-heading-subtitle"><i class="fas fa-caret-right"></i>&nbsp;
                <b>A</b>kurat Memberikan pelayanan yang teliti, seksama,cermat dan tepat benar
              </div>
            </div>
          </div>
        </div>
      </div>
      <br>
    </div>
    <div class="st-height-b120 st-height-lg-b80"></div>
  </section>
  <!-- End Tata Nilai Section -->
  <div class="st-height-b50 st-height-lg-b80"></div>

<?php
    $this->load->view('footer');
?>