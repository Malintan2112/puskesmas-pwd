<?php
  $this->load->view('navbar', $menu);
  $this->load->view('header', $title);
?>

  <div class="container">
    <br>
    <div class="row">
      <div class="col-lg-12">
        <h3 style="border-bottom: rgb(17, 193, 178) 0.4px solid; padding-bottom: 12px">Subkategori</h3>
        <table style="width: 100%;">
          <tr>
            <td><a href="<?php echo base_url(); ?>visi_misi">Visi Misi</a></td>
          </tr>
          <tr>
            <td><a href="<?php echo base_url(); ?>tata_nilai">Tata Nilai</a></td>
          </tr>
          <tr>
            <td><a href="<?php echo base_url(); ?>motto">Motto</a></td>
          </tr>
          <tr>
            <td><a href="<?php echo base_url(); ?>struktur_organisasi">Struktur Organisasi</a></td>
          </tr>
        </table>
      </div>
    </div>
    <br><br>
  </div>

  <div class="st-height-b50 st-height-lg-b80"></div>

<?php
    $this->load->view('footer');
?>