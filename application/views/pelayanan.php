<?php
    $this->load->view('navbar', $menu);
    $this->load->view('header', $title);
    ?>

    <div class="container">
      <br>
      <div class="row">
        <div class="col-lg-12">
          <h3>Jenis Pelayanan Puskesmas</h3>
          <?php foreach ($data as $pelayanan): echo htmlspecialchars_decode(stripcslashes($pelayanan['isi'])); endforeach; ?>
        </div>
      </div>
    </div>
    <div class="st-height-b50 st-height-lg-b80"></div>

<?php
    $this->load->view('footer');
?>