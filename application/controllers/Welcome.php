<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	public function index(){
		$menu = 'home';
		$dataBackground = $this->Background_Model->Get('tb_background');
		$dataArtikel = $this->Artikel_Model->Get3_latest('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'dataBackground' => $dataBackground,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('home', $data);
	}

}
