<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {
	
	public function index(){
		$menu = 'profil';
		$title = 'Profil';
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('profil', $data);
	}

}
