<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rawat_jalan extends CI_Controller {
	
	public function index(){
		$menu = 'informasi';
		$title = 'Pelayanan Rawat Jalan';
		$database = $this->Persalinan_Model->Get('tb_rawat_jalan');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'data' => $database,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('rawat_jalan', $data);
	}

}
