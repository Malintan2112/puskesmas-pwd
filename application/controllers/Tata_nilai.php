<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tata_nilai extends CI_Controller {
	
	public function index(){
		$menu = 'profil';
		$title = 'Tata Nilai';
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('tata_nilai', $data);
	}

}
