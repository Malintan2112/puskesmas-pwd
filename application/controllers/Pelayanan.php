<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan extends CI_Controller {
	
	public function index(){
		$menu = 'pelayanan';
		$title = 'Pelayanan';
		$database = $this->Pelayanan_Model->Get('tb_pelayanan');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'data'      => $database,
			'dataArtikel'      => $dataArtikel,
		);
		$this->load->view('pelayanan', $data);
	}

}
