<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motto extends CI_Controller {
	
	public function index(){
		$menu = 'profil';
		$title = 'Motto';
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('motto', $data);
	}

}
