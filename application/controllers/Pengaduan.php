<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan extends CI_Controller {
	
	public function index(){
		$menu = 'pengaduan';
		$title = 'Pengaduan Saran';
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('pengaduan', $data);
	}

	public function aksi_tambah(){
		date_default_timezone_set('Asia/Jakarta');
		$nama_ciri = $this->input->post('nama_ciri');
		$ruangan = $this->input->post('ruangan');
		$kritik_saran = $this->input->post('kritik_saran');
		$tanggal = date('Y:m:d H:i:s');
		$data = array(
			'nama_ciri' => $nama_ciri,
			'ruangan' => $ruangan,
			'kritik_saran' => $kritik_saran,
			'tanggal' => $tanggal
		);
		$this->Pengaduan_Model->insert($data);
		echo "<script>
		alert('Pengaduan Anda Berhasil Terkirim !');
		window.location.href='../pengaduan';
		</script>";
	}

}
