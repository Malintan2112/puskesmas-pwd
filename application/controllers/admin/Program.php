<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Program_Model->Get('tb_program');
		$data = array('data' => $data);
		$this->load->view('admin/program', $data);
	}

	public function edit($id){
		$program=($this->Program_Model->ambil_data_id($id));
		$data = array(
		'id'   => set_value('id',$program->id),
		'isi'       => set_value('isi',$program->isi),
		'action' 	  => site_url('admin/program/updateData')
		);
		$this->load->view('admin/program_edit', $data);
	}

	public function updateData(){
		$id = $this->input->post('id');
		$isi = $this->input->post('isi');
		$case = array('id' => $id);
		$data = array(
			'isi' => addslashes($isi),
		);
		$this->Program_Model->updateProgram($data, $case);
		redirect('admin/program');
	}

}
