<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Artikel_Model->Get('tb_artikel');
		$data = array('data' => $data);
		$this->load->view('admin/artikel_list', $data);
	}

	public function tambah(){
		return $this->load->view('admin/artikel_tambah');
	}

	private function upload(){
		date_default_timezone_set('Asia/Jakarta');
		$config['upload_path'] = './assets/artikel';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 5; // <= 5Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($this->upload->do_upload("file")){
			return $this->upload->data("file_name");
		} 
		else {
			return $this->upload->display_errors();
		}
	}

	public function aksi_tambah(){
		date_default_timezone_set('Asia/Jakarta');
		$judul = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$tayang = $this->input->post('tayang');
		$tanggal = date('Y:m:d H:i:s');
		$file = $this->upload();
		if (strpos($this->upload->display_errors(), 'large')) {
			echo "<script>
			alert('Gambar yang anda upload lebih dari 5 Mb!');
			window.location.href='../artikel/tambah';
			</script>";
		} else if(strpos($this->upload->display_errors(), 'filetype')) {
			echo "<script>
			alert('File yang anda upload bukan gambar!');
			window.location.href='../artikel/tambah';
			</script>";
		} else {
			$id_artikel = str_replace(array( '.jpg', '.jpeg', '.png' , '.PNG'), '', $file);
			$data = array(
				'id_artikel' => $id_artikel,
				'judul' => $judul,
				'gambar' => $file,
				'isi' => addslashes($isi),
				'tayang' => $tayang,
				'tanggal' => $tanggal,
			);
			$this->Artikel_Model->insert($data);
			echo "<script>
			alert('Artikel Berhasil Ditambahkan !');
			window.location.href='../artikel';
			</script>";
		}
	}

	public function notification($notif){
		$alert = "<script type='text/javascript'>alert('hello');</script>";
		return $alert;
	}

	public function detail($id){
		$artikel=($this->Artikel_Model->ambil_data_id($id));
		$data = array(
			'id_artikel'   => set_value('id_artikel',$artikel->id_artikel),
			'judul' => set_value('judul',$artikel->judul),
			'isi'       => set_value('isi',$artikel->isi),
			'tayang'       => set_value('tayang',$artikel->tayang),
			'tanggal'   => set_value('tanggal',$artikel->tanggal),
			'gambar'   => set_value('gambar',$artikel->gambar)
		);
		$this->load->view('admin/artikel_detail', $data);
	}

	public function edit($id){
		$artikel=($this->Artikel_Model->ambil_data_id($id));
		$data = array(
		'id_artikel'   => set_value('id_artikel',$artikel->id_artikel),
		'judul' => set_value('judul',$artikel->judul),
		'isi'       => set_value('isi',$artikel->isi),
		'tayang'       => set_value('tayang',$artikel->tayang),
		'gambar'   => set_value('gambar',$artikel->gambar),
		'action' 	  => site_url('admin/artikel/updateData')
		);
		$this->load->view('admin/artikel_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$id = $this->input->post('id_artikel');
		$judul = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$tayang = $this->input->post('tayang');
		$tanggal = date('Y:m:d H:i:s');
		$path = './assets/artikel/';
		$case = array('id_artikel' => $id);

		$config['upload_path'] = './assets/artikel';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 5; // <= 5Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->upload->initialize($config);

		if(!empty($_FILES['file']['name'])){
			if($this->upload->do_upload('file')){
				$file = $this->upload->data();
				$data = array(
					'judul' => $judul,
					'gambar' => $file['file_name'],
					'isi' => addslashes($isi),
					'tayang' => $tayang,
					'tanggal' => $tanggal,
				);

				@unlink($path.$this->input->post('file-lama'));
				$this->Artikel_Model->updateArtikel($data, $case);
				echo "<script>
				alert('Artikel Berhasil Diupdate !');
				window.location.href='../artikel';
				</script>";
			}else{
				echo "<script>
				alert('File yang anda upload bukan gambar atau lebih dari 5 Mb!');
				window.location.href='../artikel/edit/$id';
				</script>";
			} 
		}else{
			$data = array(
				'judul' => $judul,
				'isi' => $isi,
				'tayang' => $tayang,
				'tanggal' => $tanggal
			);
			$this->Artikel_Model->updateArtikel($data, $case);
			echo "<script>
			alert('Artikel Berhasil Diupdate !');
			window.location.href='../artikel';
			</script>";
		}
	}

	public function hapus(){
		$path = 'assets/artikel/';
		@unlink($path.$_GET['file']);

		$where = array('id_artikel' => $_GET['id']);
		$this->Artikel_Model->delete($where);
		echo "<script>
		alert('Artikel Berhasil Dihapus !');
		window.location.href='../artikel';
		</script>";
	}

}
