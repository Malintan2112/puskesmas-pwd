<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Galeri_Model->Get('tb_galeri');
		$data = array('data' => $data);
		$this->load->view('admin/galeri_list', $data);
	}

	public function tambah(){
		return $this->load->view('admin/galeri_tambah');
	}

	private function upload(){
		date_default_timezone_set('Asia/Jakarta');
		$config['upload_path'] = './assets/galeri';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 5; // <= 5Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($this->upload->do_upload("file")){
			return $this->upload->data("file_name");
		} 
		else {
			return $this->upload->display_errors();
		}
	}

	public function aksi_tambah(){
		date_default_timezone_set('Asia/Jakarta');
		$keterangan = $this->input->post('keterangan');
		$tayang = $this->input->post('tayang');
		$tanggal = date('Y:m:d H:i:s');
		$file = $this->upload();
		if (strpos($this->upload->display_errors(), 'large')) {
			echo "<script>
			alert('Gambar yang anda upload lebih dari 5 Mb!');
			window.location.href='../galeri/tambah';
			</script>";
		} else if(strpos($this->upload->display_errors(), 'filetype')) {
			echo "<script>
			alert('File yang anda upload bukan gambar!');
			window.location.href='../galeri/tambah';
			</script>";
		} else {
			$id_galeri = str_replace(array( '.jpg', '.jpeg', '.png' , '.PNG'), '', $file);
			$data = array(
				'id_galeri' => $id_galeri,
				'keterangan' => $keterangan,
				'gambar' => $file,
				'tayang' => $tayang,
				'tanggal' => $tanggal,
			);
			$this->Galeri_Model->insert($data);
			echo "<script>
			alert('Galeri Berhasil Ditambahkan !');
			window.location.href='../galeri';
			</script>";
		}
	}

	public function edit($id){
		$galeri=($this->Galeri_Model->ambil_data_id($id));
		$data = array(
		'id_galeri'   => set_value('id_galeri',$galeri->id_galeri),
		'keterangan' => set_value('keterangan',$galeri->keterangan),
		'tayang' => set_value('tayang',$galeri->tayang),
		'gambar'   => set_value('gambar',$galeri->gambar),
		'action' 	  => site_url('admin/galeri/updateData')
		);
		$this->load->view('admin/galeri_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$id = $this->input->post('id_galeri');
		$keterangan = $this->input->post('keterangan');
		$tayang = $this->input->post('tayang');
		$tanggal = date('Y:m:d H:i:s');
		$path = './assets/galeri/';
		$case = array('id_galeri' => $id);

		$config['upload_path'] = './assets/galeri';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 5; // <= 5Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->upload->initialize($config);

		if(!empty($_FILES['file']['name'])){
			if($this->upload->do_upload('file')){
				$file = $this->upload->data();
				$data = array(
					'keterangan' => $keterangan,
					'tayang' => $tayang,
					'tanggal' => $tanggal,
					'gambar' => $file['file_name'],
				);

				@unlink($path.$this->input->post('file-lama'));
				$this->Galeri_Model->updateGaleri($data, $case);
				echo "<script>
				alert('Galeri Berhasil Diupdate !');
				window.location.href='../galeri';
				</script>";
			}else{
				echo "<script>
				alert('File yang anda upload bukan gambar atau lebih dari 5 Mb!');
				window.location.href='../galeri/edit/$id';
				</script>";
			}
		}else{
			$data = array(
				'keterangan' => $keterangan,
				'tayang' => $tayang,
				'tanggal' => $tanggal
			);
			$this->Galeri_Model->updateGaleri($data, $case);
			echo "<script>
			alert('Galeri Berhasil Diupdate !');
			window.location.href='../galeri';
			</script>";
		}
	}

	public function hapus(){
		$path = 'assets/galeri/';
		@unlink($path.$_GET['file']);

		$where = array('id_galeri' => $_GET['id']);
		$this->Galeri_Model->delete($where);
		echo "<script>
		alert('Galeri Berhasil Dihapus !');
		window.location.href='../galeri';
		</script>";
	}

}
