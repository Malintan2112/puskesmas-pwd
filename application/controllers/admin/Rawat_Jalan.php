<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rawat_jalan extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Rawatjalan_Model->Get('tb_rawat_jalan');
		$data = array('data' => $data);
		$this->load->view('admin/rawat_jalan_list', $data);
	}

	public function tambah(){
		return $this->load->view('admin/rawat_jalan_tambah');
	}

	private function upload(){
		date_default_timezone_set('Asia/Jakarta');
		$config['upload_path'] = './assets/general';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 5; // <= 5Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($this->upload->do_upload("file")){
			return $this->upload->data("file_name");
		} 
		else {
			return $this->upload->display_errors();
		}
	}

	public function aksi_tambah(){
		date_default_timezone_set('Asia/Jakarta');
		$judul = $this->input->post('judul');
		$tanggal = date('Y:m:d H:i:s');
		$file = $this->upload();
		if (strpos($this->upload->display_errors(), 'large')) {
			echo "<script>
			alert('Gambar yang anda upload lebih dari 5 Mb!');
			window.location.href='../rawat_jalan/tambah';
			</script>";
		} else if(strpos($this->upload->display_errors(), 'filetype')) {
			echo "<script>
			alert('File yang anda upload bukan gambar!');
			window.location.href='../rawat_jalan/tambah';
			</script>";
		} else {
			$id_rawat_jalan = str_replace(array( '.jpg', '.jpeg', '.png' , '.PNG'), '', $file);
			$data = array(
				'id_rawat_jalan' => $id_rawat_jalan,
				'judul' => $judul,
				'gambar' => $file,
				'tanggal' => $tanggal,
			);
			$this->Rawatjalan_Model->insert($data);
			echo "<script>
			alert('Data Rawat Jalan Berhasil Ditambahkan !');
			window.location.href='../rawat_jalan';
			</script>";
		}
	}

	public function edit($id){
		$rawat_jalan=($this->Rawatjalan_Model->ambil_data_id($id));
		$data = array(
            'id_rawat_jalan'   => set_value('id_rawat_jalan',$rawat_jalan->id_rawat_jalan),
            'judul' => set_value('judul',$rawat_jalan->judul),
            'gambar'   => set_value('gambar',$rawat_jalan->gambar),
            'action' 	  => site_url('admin/rawat_jalan/updateData')
		);
		$this->load->view('admin/rawat_jalan_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$id = $this->input->post('id_rawat_jalan');
		$judul = $this->input->post('judul');
		$tanggal = date('Y:m:d H:i:s');
		$path = './assets/general/';
		$case = array('id_rawat_jalan' => $id);

		$config['upload_path'] = './assets/general';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 5; // <= 5Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->upload->initialize($config);

		if(!empty($_FILES['file']['name'])){
			if($this->upload->do_upload('file')){
				$file = $this->upload->data();
				$data = array(
					'judul' => $judul,
					'tanggal' => $tanggal,
					'gambar' => $file['file_name'],
				);

				@unlink($path.$this->input->post('file-lama'));
				$this->Rawatjalan_Model->updateRawatJalan($data, $case);
				echo "<script>
				alert('Data Rawat Jalan Berhasil Diupdate !');
				window.location.href='../rawat_jalan';
				</script>";
			}else{
				echo "<script>
				alert('File yang anda upload bukan gambar atau lebih dari 5 Mb!');
				window.location.href='../rawat_jalan/edit/$id';
				</script>";
			}
		}else{
			$data = array(
				'judul' => $judul,
				'tanggal' => $tanggal
			);
			$this->Rawatjalan_Model->updateRawatJalan($data, $case);
			echo "<script>
			alert('Data Rawat Jalan Berhasil Diupdate !');
			window.location.href='../rawat_jalan';
			</script>";
		}
	}

	public function hapus(){
		$path = 'assets/general/';
		@unlink($path.$_GET['file']);

		$where = array('id_rawat_jalan' => $_GET['id']);
		$this->Rawatjalan_Model->delete($where);
		echo "<script>
		alert('Data Rawat Jalan Berhasil Dihapus !');
		window.location.href='../rawat_jalan';
		</script>";
	}

}