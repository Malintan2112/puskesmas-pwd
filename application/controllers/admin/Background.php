<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Background extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Background_Model->Get('tb_background');
		$data = array('data' => $data);
		$this->load->view('admin/background', $data);
	}

	public function detail($id){
		$background=($this->Background_Model->ambil_data_id($id));
		$data = array(
			'id'   => set_value('id',$background->id),
			'gambar'   => set_value('gambar',$background->gambar)
		);
		$this->load->view('admin/background_detail', $data);
	}

	public function edit($id){
		$background=($this->Background_Model->ambil_data_id($id));
		$data = array(
            'id'   => set_value('id',$background->id),
            'gambar'   => set_value('gambar',$background->gambar),
            'action' 	  => site_url('admin/background/updateData')
		);
		$this->load->view('admin/background_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$id = $this->input->post('id');
		$tanggal = date('Y:m:d H:i:s');
		$path = './assets/background/';
		$case = array('id' => $id);

		$config['upload_path'] = './assets/background';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 5; // <= 5Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->upload->initialize($config);

		if(!empty($_FILES['file']['name'])){
			if($this->upload->do_upload('file')){
				$file = $this->upload->data();
				$data = array(
					'tanggal' => $tanggal,
					'gambar' => $file['file_name'],
				);

				@unlink($path.$this->input->post('file-lama'));
				$this->Background_Model->updateBackground($data, $case);
				echo "<script>
				alert('Background Berhasil Diupdate !');
				window.location.href='../background';
				</script>";
			}else{
				echo "<script>
				alert('File yang anda upload bukan gambar atau lebih dari 5 Mb!');
				window.location.href='../background/edit/$id';
				</script>";
			} 
		}else{
			echo "<script>
			alert('Tidak ada perubahan data !');
			window.location.href='../background';
			</script>";
		}
	}

}
