<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persalinan extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Persalinan_Model->Get('tb_persalinan');
		$data = array('data' => $data);
		$this->load->view('admin/persalinan_list', $data);
	}

	public function tambah(){
		return $this->load->view('admin/persalinan_tambah');
	}

	private function upload(){
		date_default_timezone_set('Asia/Jakarta');
		$config['upload_path'] = './assets/general';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 5; // <= 5Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($this->upload->do_upload("file")){
			return $this->upload->data("file_name");
		} 
		else {
			return $this->upload->display_errors();
		}
	}

	public function aksi_tambah(){
		date_default_timezone_set('Asia/Jakarta');
		$judul = $this->input->post('judul');
		$tanggal = date('Y:m:d H:i:s');
		$file = $this->upload();

		if (strpos($this->upload->display_errors(), 'large')) {
			echo "<script>
			alert('Gambar yang anda upload lebih dari 5 Mb!');
			window.location.href='../persalinan/tambah';
			</script>";
		} else if(strpos($this->upload->display_errors(), 'filetype')) {
			echo "<script>
			alert('File yang anda upload bukan gambar!');
			window.location.href='../persalinan/tambah';
			</script>";
		} else {
			$id_persalinan = str_replace(array( '.jpg', '.jpeg', '.png' , '.PNG'), '', $file);
			$data = array(
				'id_persalinan' => $id_persalinan,
				'judul' => $judul,
				'gambar' => $file,
				'tanggal' => $tanggal,
			);
			$this->Persalinan_Model->insert($data);
			echo "<script>
			alert('Data Persalinan Berhasil Ditambahkan !');
			window.location.href='../persalinan';
			</script>";
		}
	}

	public function edit($id){
		$persalinan=($this->Persalinan_Model->ambil_data_id($id));
		$data = array(
            'id_persalinan'   => set_value('id_persalinan',$persalinan->id_persalinan),
            'judul' => set_value('judul',$persalinan->judul),
            'gambar'   => set_value('gambar',$persalinan->gambar),
            'action' 	  => site_url('admin/persalinan/updateData')
		);
		$this->load->view('admin/persalinan_edit', $data);
	}

	public function updateData(){
		date_default_timezone_set('Asia/Jakarta');
		$id = $this->input->post('id_persalinan');
		$judul = $this->input->post('judul');
		$tanggal = date('Y:m:d H:i:s');
		$path = './assets/general/';
		$case = array('id_persalinan' => $id);

		$config['upload_path'] = './assets/general';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024 * 5; // <= 5Mb;
		$config['file_name'] = uniqid(date('dmY') . '_');

		$this->upload->initialize($config);

		if(!empty($_FILES['file']['name'])){
			if($this->upload->do_upload('file')){
				$file = $this->upload->data();
				$data = array(
					'judul' => $judul,
					'tanggal' => $tanggal,
					'gambar' => $file['file_name'],
				);

				@unlink($path.$this->input->post('file-lama'));
				$this->Persalinan_Model->updatePersalinan($data, $case);
				echo "<script>
				alert('Data Persalinan Berhasil Diupdate !');
				window.location.href='../persalinan';
				</script>";
			}else{
				echo "<script>
				alert('File yang anda upload bukan gambar atau lebih dari 5 Mb!');
				window.location.href='../persalinan/edit/$id';
				</script>";
			}
		}else{
			$data = array(
				'judul' => $judul,
				'tanggal' => $tanggal
			);
			$this->Persalinan_Model->updatePersalinan($data, $case);
			echo "<script>
			alert('Data Persalinan Berhasil Diupdate !');
			window.location.href='../persalinan';
			</script>";
		}
	}

	public function hapus(){
		$path = 'assets/general/';
		@unlink($path.$_GET['file']);

		$where = array('id_persalinan' => $_GET['id']);
		$this->Persalinan_Model->delete($where);
		echo "<script>
		alert('Data Persalinan Berhasil Dihapus !');
		window.location.href='../persalinan';
		</script>";
	}

}
