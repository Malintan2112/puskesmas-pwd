<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan extends CI_Controller {

	public function __construct(){
		parent::__construct();
        //load model admin
		$this->load->model('admin');
		$this->load->library('upload');

        //cek session user
		if($this->admin->is_role() != "admin"){
			redirect("login");
		}
	}

	public function index(){
		$data = $this->Pelayanan_Model->Get('tb_pelayanan');
		$data = array('data' => $data);
		$this->load->view('admin/pelayanan', $data);
	}

	public function edit($id){
		$pelayanan=($this->Pelayanan_Model->ambil_data_id($id));
		$data = array(
		'id'   => set_value('id',$pelayanan->id),
		'isi'       => set_value('isi',$pelayanan->isi),
		'action' 	  => site_url('admin/pelayanan/updateData')
		);
		$this->load->view('admin/pelayanan_edit', $data);
	}

	public function updateData(){
		$id = $this->input->post('id');
		$isi = $this->input->post('isi');
		$case = array('id' => $id);
		$data = array(
			'isi' => addslashes($isi),
		);
		$this->Pelayanan_Model->updatePelayanan($data, $case);
		redirect('admin/pelayanan');
	}

}
