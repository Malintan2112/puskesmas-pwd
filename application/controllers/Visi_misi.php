<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visi_misi extends CI_Controller {
	
	public function index(){
		$menu = 'profil';
		$title = 'Visi Misi';
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('visi_misi', $data);
	}

}
