<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends CI_Controller {
	
	public function index(){
		$menu = 'informasi';
		$title = 'Galeri';
		$database = $this->Galeri_Model->GetFE('tb_galeri');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'data' => $database,
			'menu' 	   => $menu,
			'title'      => $title,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('galeri', $data);
	}

}
