<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {
	
	public function index(){
		$menu = 'program';
		$title = 'Program';
		$database = $this->Program_Model->Get('tb_program');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'data'      => $database,
			'dataArtikel'      => $dataArtikel,
		);
		$this->load->view('program', $data);
	}

}
