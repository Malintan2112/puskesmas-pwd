<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends CI_Controller {
	
	public function index(){
		$database = $this->Artikel_Model->GetFE('tb_artikel');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$menu = 'informasi';
		$title = 'Informasi';
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'data'      => $database,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('informasi', $data);
	}

}
