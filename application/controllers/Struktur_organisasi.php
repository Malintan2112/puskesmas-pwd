<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Struktur_organisasi extends CI_Controller {
	
	public function index(){
		$menu = 'profil';
		$title = 'Struktur Organisasi';
		$data = $this->Struktur_Model->Get('tb_struktur');
		$dataArtikel = $this->Artikel_Model->GetFE('tb_artikel');
		$data = array(
			'menu' 	   => $menu,
			'title'      => $title,
			'data'      => $data,
			'dataArtikel' => $dataArtikel,
		);
		$this->load->view('struktur_organisasi', $data);
	}

}
