-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 14, 2022 at 07:25 PM
-- Server version: 10.5.16-MariaDB-cll-lve
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pusk7069_puskesmas`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_akun`
--

CREATE TABLE `tb_akun` (
  `id_akun` int(8) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `ket` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` enum('admin') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_akun`
--

INSERT INTO `tb_akun` (`id_akun`, `nama`, `ket`, `username`, `password`, `level`) VALUES
(1, 'Admin ', 'Developer', 'ibra', '0b5ffc09eb62ef2241f07327276ee064', 'admin'),
(2, 'Hernawan', 'Pengelola', 'malintan', '4788e0300fc11c924e36ef886df8ba3c', 'admin'),
(3, 'Admin Puskesmas Pwd 1', 'Pengelola', 'puskesmaspwd1', '364f3b1fe14fdf9aaa8f62e053cacb34', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tb_artikel`
--

CREATE TABLE `tb_artikel` (
  `no` int(8) NOT NULL,
  `id_artikel` varchar(50) NOT NULL,
  `judul` varchar(40) NOT NULL,
  `isi` longtext NOT NULL,
  `tanggal` datetime NOT NULL,
  `gambar` text NOT NULL,
  `tayang` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_artikel`
--

INSERT INTO `tb_artikel` (`no`, `id_artikel`, `judul`, `isi`, `tanggal`, `gambar`, `tayang`) VALUES
(24, '07082022_62ef9a47bf7c5', 'testing', '                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.                                                                                                                                                                                                                        ', '2022-08-14 18:51:03', '14082022_62f8d964bb846.jpeg', 0),
(25, '07082022_62ef9a80ea7c8', 'budi contoh judul', '                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum                                    ', '2022-08-14 18:20:24', '14082022_62f8d7937a325.jpg', 0),
(26, '07082022_62ef9a98d1aed', 'contoh judul', '                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum                                    ', '2022-08-14 18:20:17', '14082022_62f8d7657036a.jpg', 0),
(27, '07082022_62ef9ad8da999', 'tes judul', '                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum                                                                                                               ', '2022-08-14 19:02:35', '14082022_62f8e379e5cfe.jpg', 0),
(28, '07082022_62ef9af17d24f', 'Lorem ipsum', '                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum                                                                                    ', '2022-08-14 18:50:56', '14082022_62f8d1ac4cc45.jpg', 0),
(30, '07082022_62efabe402575', 'faq', '                                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum                                                                                                      ', '2022-08-14 19:02:29', '14082022_62f7fa68d2dd6.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_galeri`
--

CREATE TABLE `tb_galeri` (
  `no` int(8) NOT NULL,
  `id_galeri` varchar(40) NOT NULL,
  `keterangan` varchar(40) NOT NULL,
  `tanggal` datetime NOT NULL,
  `gambar` varchar(40) NOT NULL,
  `tayang` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_galeri`
--

INSERT INTO `tb_galeri` (`no`, `id_galeri`, `keterangan`, `tanggal`, `gambar`, `tayang`) VALUES
(2, '07082022_62efc9465f5fc', 'bangunan puskesmas', '2022-08-14 19:04:10', '14082022_62f8e4ba17f7f.jpg', 0),
(3, '07082022_62efc95333562', 'coba', '2022-08-14 19:03:46', '14082022_62f8e4a2b8007.jpg', 0),
(4, '07082022_62efc982ad23a', 'ted', '2022-08-14 19:03:33', '14082022_62f8e4955fbec.jpg', 0),
(5, '07082022_62efc997d373c', 'cek rontgen', '2022-08-14 19:03:03', '14082022_62f8e47790803.jpg', 0),
(6, '07082022_62efc9a7a015d', 'diskusi', '2022-08-14 19:02:53', '14082022_62f8e46d9648f.jpeg', 0),
(7, '07082022_62efc9b4a1c57', 'senyumd', '2022-08-14 19:07:42', '14082022_62f7f3cb4c10f.jpeg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaduan`
--

CREATE TABLE `tb_pengaduan` (
  `id_pengaduan` int(8) NOT NULL,
  `tanggal` datetime NOT NULL,
  `nama_ciri` varchar(50) NOT NULL,
  `ruangan` varchar(30) NOT NULL,
  `kritik_saran` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_program`
--

CREATE TABLE `tb_program` (
  `id` int(11) NOT NULL,
  `isi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_program`
--

INSERT INTO `tb_program` (`id`, `isi`) VALUES
(1, '                                                      <ol style=\"margin-bottom: 15px; -webkit-font-smoothing: antialiased; padding-left: 15px; color: rgb(102, 102, 102); font-family: Montserrat, sans-serif; font-size: 15px;\"><li style=\"-webkit-font-smoothing: antialiased;\">Upaya Kesehatan Masyarakat<ol style=\"-webkit-font-smoothing: antialiased; padding-left: 15px; margin-left: 20px;\"><li style=\"-webkit-font-smoothing: antialiased; list-style-type: upper-alpha;\">Esensial<ol style=\"-webkit-font-smoothing: antialiased; padding-left: 15px; margin-left: 20px;\"><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Upaya Kesehatan Masyarakat</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Kesehatan Lingkungan</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Kesehatan Ibu dan Anak termasuk Keluarga Berencana</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Gizi</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Pencegahan dan Pengendalian Penyakit</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Kesehatan Masyarakat (Perkesmas)</li></ol></li></ol><ol start=\"2\" style=\"-webkit-font-smoothing: antialiased; padding-left: 15px; margin-left: 20px;\"><li style=\"-webkit-font-smoothing: antialiased; list-style-type: upper-alpha;\">Pengembangan<ol style=\"-webkit-font-smoothing: antialiased; padding-left: 15px; margin-left: 20px;\"><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Kesehatan Lanjut Usia</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Kesehatan Peduli Remaja (PKPR)</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Usaha Kesehatan Sekolah</li></ol></li></ol></li><li style=\"-webkit-font-smoothing: antialiased; margin-top: 10px;\">Upaya Kesehatan Perorangan<ol style=\"-webkit-font-smoothing: antialiased; padding-left: 15px; margin-left: 20px;\"><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Pemeriksaan Umum</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Lanjut Usia</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Kesehatan Ibu, Anak dan Keluarga Berencana</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Kesehatan Gigi dan Mulut</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan MTBS</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Imunisasi</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Persalinan 24 jam</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Tindakan</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Laboratorium</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Kefarmasian</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Fisioterapi</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Gigi</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan Kesehatan Peduli Remaja</li><li style=\"-webkit-font-smoothing: antialiased;\">Pelayanan TB/HIV</li></ol></li></ol>                                                ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_akun`
--
ALTER TABLE `tb_akun`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indexes for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `tb_galeri`
--
ALTER TABLE `tb_galeri`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `tb_pengaduan`
--
ALTER TABLE `tb_pengaduan`
  ADD PRIMARY KEY (`id_pengaduan`);

--
-- Indexes for table `tb_program`
--
ALTER TABLE `tb_program`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_akun`
--
ALTER TABLE `tb_akun`
  MODIFY `id_akun` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  MODIFY `no` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_galeri`
--
ALTER TABLE `tb_galeri`
  MODIFY `no` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_pengaduan`
--
ALTER TABLE `tb_pengaduan`
  MODIFY `id_pengaduan` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_program`
--
ALTER TABLE `tb_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
